/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.net.BindException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.servlet.mvc.SimpleFormController;
import servicio.HelloService;

/**
 *
 * @author ESMERALDA
 */
public class HelloController extends SimpleFormController {
    
    private HelloService helloService;
    
    public void setHelloService(HelloService helloService) {
    this.helloService = helloService;
}
    
    public HelloController() {
        //Initialize controller properties here or 
        //in the Web Application Context

    setCommandClass ( nombre.class);
    setCommandName (" nombre ");
    setSuccessView (" hola Vista");
    setFormView (" Vista de nombre "); 
    
    
    }
    
   
     protected ModelAndView onSubmit(
     HttpServletRequest request, 
     HttpServletResponse response, 
     Object command, 
     BindException errors) throws Exception {
     
     
        nombre name = (nombre) command;
        ModelAndView mv = new ModelAndView(getSuccessView());
        mv.addObject("helloMessage", helloService.sayHello(name.getValue()));
        return mv;
     }
     
}
